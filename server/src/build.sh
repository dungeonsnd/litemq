#!/bin/sh

cd common
make clean
make -j4
cd ../

cd conf
make clean
make -j4
cd ../

cd context
make clean
make -j4
cd ../

cd protocol
make clean
make -j4
cd ../

cd logic
#make clean
#make -j4
cd ../

cd network
make clean
make -j4
cd ../

make clean
make -j4
