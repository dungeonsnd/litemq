
#include "protocol/lmq_protocol_def_packet.hpp"
#include "lpr/lpr_common_include_system.hpp"

namespace lmq
{

Packet::Packet()
{
    Reset();
}
Packet::~Packet()
{
}

void Packet::Reset()
{
    _packetStatus =PS_WAIT_FIX0;
    _packetData.resize(0);    
    _lenReservedRegion =0;
    _lenHeader =0;
    _lenBody =0;
    _packetRecvedFinished =0;
}

int Packet::GetWaitLength()
{
    int res =-1;
    switch (_packetStatus)
    {
    case PS_WAIT_FIX0:
        res =PL_FIX0;
        break;
    case PS_WAIT_VAR0:
        res =_lenReservedRegion;
        break;
    case PS_WAIT_FIX1:
        res =PL_FIX1;
        break;
    case PS_WAIT_VAR1:
        res =_lenHeader+_lenBody;
        break;
    default:
        break;
    }
    return res;
}

std::string & Packet::GetPacketData()
{
    return _packetData;
}

int Packet::Parse()
{
    int res =0;
    switch (_packetStatus)
    {
    case PS_WAIT_FIX0:
        if(_packetData.size()>=PS_WAIT_FIX0)
        {
            _lenReservedRegion =_packetData[PS_WAIT_FIX0-1];
            _packetStatus =PS_WAIT_FIX0;
        }
        break;
    case PS_WAIT_VAR0:
        if(_packetData.size()>=PS_WAIT_FIX0+_lenReservedRegion)
        {
            _packetStatus =PS_WAIT_VAR0;
        }
        break;
    case PS_WAIT_FIX1:
        if(_packetData.size()>=PS_WAIT_FIX0+_lenReservedRegion+PS_WAIT_FIX1)
        {
            uint32_t * p =(uint32_t*)(&_packetData[PS_WAIT_FIX0+_lenReservedRegion]);
            _lenHeader =ntohl(*p);
            p =(uint32_t*)(&_packetData[PS_WAIT_FIX0+_lenReservedRegion+_lenHeader]);
            _lenBody =ntohl(*p);
            _packetStatus =PS_WAIT_VAR1;
            if(_lenHeader>10485760 || _lenBody>104857600)
            {
                res =-1;
            }
        }
        break;
    case PS_WAIT_VAR1:
        if(_packetData.size()>=PS_WAIT_FIX0+_lenReservedRegion+PS_WAIT_FIX1+_lenHeader+_lenBody)
        {
            _packetStatus =PS_WAIT_FIX0;
            _packetRecvedFinished =1;
        }
        break;
    default:
        res =1;
        break;
    }
    return res;
}

int Packet::IsPacketFinished()
{
    return _packetRecvedFinished;
}
    
} // namespace lmq

