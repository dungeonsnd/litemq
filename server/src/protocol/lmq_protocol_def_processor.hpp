#ifndef _HEADER_FILE_LMQ_PROTOCOL_PROCESSOR_HPP_
#define _HEADER_FILE_LMQ_PROTOCOL_PROCESSOR_HPP_

#include "lpr/lpr_common_include_cpp.hpp"
#include "lpr/lpr_session_define.hpp"

int lmq_protocol_def_processor_readcb(lpr::Session * sess, char errmsg[LPR_ERR_SIZE]);

#endif // _HEADER_FILE_LMQ_PROTOCOL_PROCESSOR_HPP_
