
#include "protocol/lmq_protocol_def_processor.hpp"
#include "protocol/lmq_protocol_def_packet.hpp"
#include "context/lmq_context_define.hpp"
#include "network/lmq_network_session_info.hpp"
#include "lpr/lpr_common_helper_macro.hpp"
#include "lpr/lpr_common_include_system.hpp"
#include "lpr/lpr_common_logger.hpp"

        
#define LMQ_CMD_EXIT "q"
#define LMQ_CMD_EXIT_LN "q\n"

int lmq_protocol_def_processor_mainten(lpr::Session * sess, lmq::Context * context, 
    lmq::SessionInfo * sessionInfo, lmq::Packet * packet, 
    char errmsg[LPR_ERR_SIZE])
{
    int res =-1;
    do{
        LPR_ISNULL(sess,errmsg);
        LPR_ISNULL(context,errmsg);
        LPR_ISNULL(sessionInfo,errmsg);
        LPR_ISNULL(packet,errmsg);
        
        int bufsize =sess->GetBufSize(lpr::Session::RWF_R, errmsg);
        if(-1==bufsize)
        {
            break;
        }
        else if(0==bufsize)
        {
            break;
        }
        
        std::string data(bufsize,'\0');
        int readsize =sess->Read(&data[0], bufsize, errmsg);
        if(readsize<0)
        {
            break;
        }
        else if(0==readsize)
        {
            break;
        }

        if( LPR_STR_EQ(LMQ_CMD_EXIT,data.c_str()) || LPR_STR_EQ(LMQ_CMD_EXIT_LN,data.c_str()) )
        {
            event_base_loopbreak(sess->GetBase());
            break;
        }
        
        // begin process.
        int rt =sess->Write(&data[0], bufsize, errmsg);
        if(0!=rt)
            LOG_INFO("base","Write failed! \n");

        res =0;
    }while(0);
    return res;
}

int lmq_protocol_def_processor_client(lpr::Session * sess, lmq::Context * context, 
    lmq::SessionInfo * sessionInfo, lmq::Packet * packet, 
    char errmsg[LPR_ERR_SIZE])
{
    int res =-1;
    do{
        LPR_ISNULL(sess,errmsg);
        LPR_ISNULL(context,errmsg);
        LPR_ISNULL(sessionInfo,errmsg);
        LPR_ISNULL(packet,errmsg);
        
        
        res =0;
    }while(0);
    return res;
}

int lmq_protocol_def_processor_service(lpr::Session * sess, lmq::Context * context, 
    lmq::SessionInfo * sessionInfo, lmq::Packet * packet, 
    char errmsg[LPR_ERR_SIZE])
{
    int res =-1;
    do{
        LPR_ISNULL(sess,errmsg);
        LPR_ISNULL(context,errmsg);
        LPR_ISNULL(sessionInfo,errmsg);
        LPR_ISNULL(packet,errmsg);
        
        while(1)
        {
            res =sess->GetBufSize(lpr::Session::RWF_R, errmsg);
            if(-1==res)
                break;
            
            int len =packet->GetWaitLength();
            if(res<len) // data not enough.
            {
                res =0;
                break;
            }
            
            int rt =sess->Read(packet->GetPacketData(), size_t(len),errmsg);
            if(-1==rt)
                break;
            rt =packet->Parse();
            if(-1==rt)
                break;
            if(packet->IsPacketFinished())  // process.
            {
                printf("PacketFinished!! Going to process. \n");
                packet->Reset();
            }
            else
                printf("Packet not finished ! Going to wait data. \n");
        }
    }while(0);
    return res;
}

int lmq_protocol_def_processor_readcb(lpr::Session * sess, char errmsg[LPR_ERR_SIZE])
{
    LOG_TRACE("base","Enter lmq_protocol_def_processor_readcb, sess=%p", sess);
    int res =-1;
    do{
        LPR_ISNULL(sess,errmsg);
        lmq::Context * context =(lmq::Context *)(sess->GetContext());
        LPR_ISNULL(context,errmsg);
        lmq::SessionInfo * sessionInfo =(lmq::SessionInfo *)(sess->GetSessionInfo());
        LPR_ISNULL(sessionInfo,errmsg);
        lmq::Packet * packet =(lmq::Packet *)(sess->GetPacket());
        LPR_ISNULL(packet,errmsg);
        
        lmq::SessionInfo::SessionRole sessionRole =sessionInfo->GetSessionRole();
        switch(sessionRole)
        {
        case lmq::SessionInfo::SR_MAINTEN:
            res =lmq_protocol_def_processor_mainten(sess, context, sessionInfo, packet, errmsg);
            break;
        case lmq::SessionInfo::SR_CLIENT:
            res =lmq_protocol_def_processor_client(sess, context, sessionInfo, packet, errmsg);
            break;
        case lmq::SessionInfo::SR_SERVICE:
            res =lmq_protocol_def_processor_service(sess, context, sessionInfo, packet, errmsg);
            break;
        default:
            break;
        }
    }while(0);
    if(-1==res)
    {
        delete sess;
        sess =NULL;
    }
    LOG_TRACE("base","Leave lmq_protocol_def_processor_readcb, res=%d,errmsg=%s", 
        res,errmsg);
    return res;
}
