#ifndef _HEADER_FILE_LMQ_PROTOCOL_PACKET_HPP_
#define _HEADER_FILE_LMQ_PROTOCOL_PACKET_HPP_

#include "lpr/lpr_common_include_cpp.hpp"

namespace lmq
{

enum PacketStatus
{
    PS_WAIT_FIX0 =1,
    PS_WAIT_VAR0 =2,
    PS_WAIT_FIX1 =3,
    PS_WAIT_VAR1 =4
};

enum PacketLength
{
    PL_FIX0 =8,
    PL_FIX1 =8
};


class Packet
{
public:
    Packet();
    ~Packet();
    void Reset();
    int GetWaitLength();
    std::string & GetPacketData();    
    int Parse();
    int IsPacketFinished();
private:
    PacketStatus _packetStatus;
    std::string _packetData;
    unsigned _lenReservedRegion;
    unsigned _lenHeader;
    unsigned _lenBody;
    int _packetRecvedFinished;
};

} // namespace lmq

#endif // _HEADER_FILE_LMQ_PROTOCOL_PACKET_HPP_
