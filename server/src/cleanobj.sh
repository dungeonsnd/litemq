#!/bin/sh

cd common
make cleanobj
cd ../

cd conf
make cleanobj
cd ../

cd context
make cleanobj
cd ../

cd protocol
make cleanobj
cd ../

cd network
make cleanobj
cd ../

make clean
rm ../litemq_unix/*.so
