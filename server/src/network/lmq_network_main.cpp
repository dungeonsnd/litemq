
#include "network/lmq_network_main.hpp"
#include "network/lmq_network_io_engine.hpp"
#include "network/lmq_network_session_info.hpp"
#include "protocol/lmq_protocol_def_packet.hpp"
#include "lpr/lpr_common_network.hpp"
#include "lpr/lpr_common_include_cpp.hpp"
#include "lpr/lpr_common_helper_macro.hpp"
#include "lpr/lpr_session_define.hpp"
#include "lpr/lpr_common_logger.hpp"

namespace lmq
{

void lmq_sesssion_destructor_cb(void * context, void * sessionInfo, void * packet)
{
    lmq::Packet * p =(lmq::Packet *)packet; // 后构造的先释放.
    delete p;
    p =NULL;
    lmq::SessionInfo * d =(lmq::SessionInfo *)sessionInfo;
    delete d;
    d =NULL;
}


NetworkService::NetworkService():
    _context(NULL)
{
}

NetworkService::~NetworkService()
{
    for(std::vector < event * >::iterator it= _events.begin();it!=_events.end();it++)
    {
        event_free(*it);
    }
        
    if(_context->GetBase())
        event_base_free(_context->GetBase());
}


int NetworkService::CreateServer(event_base * base, event_callback_fn cb, void *arg, 
        const char * addr)
{
    LOG_TRACE("base","Enter NetworkService::CreateServer, base=%p, cb=%p, arg=%p, addr=%s",
        base, cb, arg, addr);
    int res =-1;
    do{
        evutil_socket_t listensocket;
        if (0!=lpr::lpr_common_create_listensocket(addr, listensocket))
        {
            break;
        }
        event * listenEvent_mainten =event_new(base, listensocket, EV_READ|EV_PERSIST,
                                        cb, arg);
        event_add(listenEvent_mainten, NULL);
        _events.push_back(listenEvent_mainten);
        
        res =0;
    }while(0);
    LOG_TRACE("base","Leave NetworkService::CreateServer, res=%d \n", res);
    return res;
}

int NetworkService::Start(lmq::Context * context)
{
    int res =-1;
    do{
        lpr::Session::_destructorCb =lmq_sesssion_destructor_cb;
        
        char errmsg[LPR_ERR_SIZE];
        LPR_ISNULL(context,errmsg);
        _context =context;
        Config * conf =_context->GetConf();
        LPR_ISNULL(conf,errmsg);
        
        event_base * base =event_base_new();
        LPR_ISNULL(base,errmsg);
        if(0!=context->SetBase(base))
        {
            LOG_FATAL("base","context->SetBase failed! \n");
            break;
        }
        
        // create servers.
        if ( 0!=CreateServer(base, 
            lmq_network_acceptcb_mainten, _context, conf->mainten_addr.c_str()) )
        {
            LOG_FATAL("base","CreateServer of mainten failed! \n");
            break;
        }
        if ( 0!=CreateServer(base, 
            lmq_network_acceptcb_client, _context, conf->client_addr.c_str()) )
        {
            LOG_FATAL("base","CreateServer of client failed! \n");
            break;
        }
        if ( 0!=CreateServer(base, 
            lmq_network_acceptcb_service, _context, conf->service_addr.c_str()) )
        {
            LOG_FATAL("base","CreateServer of service failed! \n");
            break;
        }        
        
        event_base_dispatch(base);
        res =0;
    }while(0);
    return res;
}

} // namespace lmq

