#ifndef _HEADER_FILE_LMQ_NETWORK_IO_ENGINE_HPP_
#define _HEADER_FILE_LMQ_NETWORK_IO_ENGINE_HPP_

#include "lpr/lpr_common_include_libevent.hpp"

void lmq_network_readcb(bufferevent *bev, void * arg);
void lmq_network_writecb(bufferevent *bev, void * arg);
void lmq_network_eventcb(bufferevent *bev, short error, void * arg);
void lmq_network_timercb(int fd, short event, void * arg);
void lmq_network_acceptcb_mainten(evutil_socket_t listensocket, short event, void * arg);
void lmq_network_acceptcb_client(evutil_socket_t listensocket, short event, void * arg);
void lmq_network_acceptcb_service(evutil_socket_t listensocket, short event, void * arg);

#endif // _HEADER_FILE_LMQ_NETWORK_IO_ENGINE_HPP_
