
#include "network/lmq_network_io_engine.hpp"
#include "network/lmq_network_session_info.hpp"
#include "protocol/lmq_protocol_def_processor.hpp"
#include "protocol/lmq_protocol_def_packet.hpp"
#include "context/lmq_context_define.hpp"
#include "lpr/lpr_common_helper_macro.hpp"
#include "lpr/lpr_common_network.hpp"
#include "lpr/lpr_common_include_system.hpp"
#include "lpr/lpr_session_define.hpp"
#include "lpr/lpr_common_logger.hpp"

void lmq_network_readcb(bufferevent *bev, void * arg)
{
    LOG_TRACE("base","Enter lmq_network_readcb, bev=%p,arg=%p", bev,arg);
    do{
        lpr::Session * session =(lpr::Session *)arg;
        char errmsg[LPR_ERR_SIZE];
        LPR_ISNULL(session,errmsg);
        
        if(0!=lmq_protocol_def_processor_readcb(session,errmsg))
        {
            break;
        }
    }while(0);
    LOG_TRACE("base","Leave lmq_network_readcb, bev=%p,arg=%p", bev,arg);
}

void lmq_network_writecb(bufferevent *bev, void * arg)
{
    LOG_TRACE("base","Enter lmq_network_writecb, bev=%p,arg=%p", bev,arg);
    do{
        lpr::Session * session =(lpr::Session *)arg;
        char errmsg[LPR_ERR_SIZE];
        LPR_ISNULL(session,errmsg);
    }while(0);
    LOG_TRACE("base","Leave lmq_network_writecb, bev=%p,arg=%p", bev,arg);
}

void lmq_network_eventcb(bufferevent *bev, short error, void * arg)
{
    LOG_TRACE("base","Enter lmq_network_eventcb, bev=%p,arg=%p", bev,arg);
    int err =EVUTIL_SOCKET_ERROR();
    do{
        lpr::Session * session =(lpr::Session *)arg;
        char errmsg[LPR_ERR_SIZE];
        LPR_ISNULL(session,errmsg);
        
        if (error & BEV_EVENT_EOF)
        {
            LOG_TRACE("base","connection closed");
        }
        else if (error & BEV_EVENT_ERROR)
        {
            LOG_TRACE("base","some other error(%d),%s \n",err,evutil_socket_error_to_string(err));
        }
        else if (error & BEV_EVENT_TIMEOUT)
        {
            LOG_TRACE("base","Timed out\n");
        }
        delete session;
        session =NULL;
    }while(0);
    LOG_TRACE("base","Leave lmq_network_eventcb, bev=%p,arg=%p", bev,arg);
}

void lmq_network_timercb(int fd, short event, void * arg)
{
    LOG_TRACE("base","Enter lmq_network_timercb, fd=%d,event=%d,arg=%p", fd, event, arg);
    do{
    }while(0);
    LOG_TRACE("base","Leave lmq_network_timercb");
}

void lmq_network_accept(lmq::SessionInfo::SessionRole sessionRole,
    evutil_socket_t listensocket, short event, void * arg)
{
    LOG_TRACE("base","Enter lmq_network_accept, sessionRole=%d,listensocket=%d,event=%d,arg=%p", 
        sessionRole,listensocket,event,arg);
    do{
        evutil_socket_t clientsocket;
        int rt =lpr::lpr_common_accept(listensocket, clientsocket);
        if(0!=rt)
            break;
        LOG_TRACE("base","accept client clientsocket=%d \n",clientsocket);
        
        lmq::Context * context =(lmq::Context *)arg;
        char errmsg[LPR_ERR_SIZE];
        LPR_ISNULL(context,errmsg);
        event_base * base =context->GetBase();
        LPR_ISNULL(base,errmsg);
        
        lmq_newobj(sessionInfo, lmq::SessionInfo, sessionRole);
        LPR_ISNULL(sessionInfo,errmsg);
        lmq_newobj(pakcet, lmq::Packet);
        LPR_ISNULL(pakcet,errmsg);
        
        lmq_newobj(session, lpr::Session,
                    base,
                    clientsocket,
                    context,
                    sessionInfo,
                    pakcet,
                    lmq_network_readcb,
                    lmq_network_writecb,
                    lmq_network_eventcb);
        LPR_ISNULL(session,errmsg);
    }while(0);
    LOG_TRACE("base","Leave lmq_network_accept");
}

void lmq_network_acceptcb_mainten(evutil_socket_t listensocket, short event, void * arg)
{
    lmq_network_accept(lmq::SessionInfo::SR_MAINTEN, listensocket, event, arg);
}
void lmq_network_acceptcb_client(evutil_socket_t listensocket, short event, void * arg)
{
    lmq_network_accept(lmq::SessionInfo::SR_CLIENT, listensocket, event, arg);
}
void lmq_network_acceptcb_service(evutil_socket_t listensocket, short event, void * arg)
{
    lmq_network_accept(lmq::SessionInfo::SR_SERVICE, listensocket, event, arg);
}
