#ifndef _HEADER_FILE_LMQ_NETWORK_SESSION_INFO_HPP_
#define _HEADER_FILE_LMQ_NETWORK_SESSION_INFO_HPP_

namespace lmq
{

class SessionInfo
{
public:
    enum SessionRole
    {
        SR_MAINTEN =1024,
        SR_CLIENT,
        SR_SERVICE
    };

    SessionInfo(SessionRole sessionRole):
        _sessionRole(sessionRole)
    {
    }
    ~SessionInfo()
    {
    }
    
    SessionRole GetSessionRole() { return _sessionRole; }
    
private:
    SessionRole _sessionRole;
};

} // namespace lmq

#endif // _HEADER_FILE_LMQ_NETWORK_SESSION_INFO_HPP_
