
#include "lpr/lpr_common_include_libevent.hpp"
#include "lpr/lpr_common_include_cpp.hpp"
#include "conf/lmq_conf_config.hpp"
#include "context/lmq_context_define.hpp"

namespace lmq
{

class NetworkService
{
public:
    NetworkService();
    ~NetworkService();
    
    int Start(lmq::Context * context);
    
private:
    int CreateServer(event_base * base, event_callback_fn cb, void *arg, 
        const char * addr);
        
    lmq::Context * _context;
    std::vector < event * > _events;
};

} // namespace lmq

