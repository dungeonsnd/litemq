#ifndef _HEADER_FILE_LMQ_CONTEXT_DEFINE_HPP_
#define _HEADER_FILE_LMQ_CONTEXT_DEFINE_HPP_

#include "lpr/lpr_common_include_libevent.hpp"

namespace lmq
{

class Config;

class Context
{
public:
    Context();
    ~Context();
    
    int SetBase(event_base * base);
    event_base * GetBase();
    
    int SetConf(Config * conf);
    Config * GetConf();
private:
    event_base * _base;
    Config * _conf;
};

} // namespace lmq

#endif // _HEADER_FILE_LMQ_CONTEXT_DEFINE_HPP_
