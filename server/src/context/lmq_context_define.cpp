
#include "lmq_context_define.hpp"

namespace lmq
{

Context::Context():
    _base(NULL),
    _conf(NULL)
{
}

Context::~Context()
{
}

int Context::SetBase(event_base * base)
{
    if(NULL==_base)
    {
        _base =base;
        return 0;
    }
    else
    {
        return -1;
    }
}

event_base * Context::GetBase()
{
    return _base;
}


int Context::SetConf(Config * conf)
{
    if(NULL==_conf)
    {
        _conf =conf;
        return 0;
    }
    else
    {
        return -1;
    }
}

Config * Context::GetConf()
{
    return _conf;
}

    
} // namespace lmq

