
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "lmq_network_main.hpp"
#include "lmq_conf_config.hpp"
#include "lmq_context_define.hpp"
#include "lpr/lpr_common_logger.hpp"

void Usage(const char * argv0)
{
    printf("Usage: \n  %s "
           "[-f] config file  "
           "[-v] "
           "[-h] "
           "\n\n",argv0);
    printf("  -f config file "
           "  -v show version and exit"
           "  -h show help and exit "
           "\n");
}

void GetOptions(int argc,char * argv[],
                std::string & f)
{
    int opt;
    opterr =0;
    while( (opt =getopt(argc,argv,"f:vh"))!=-1 )
    {
        switch(opt)
        {
        case 'f':
            f =optarg;
            break;
        case 'v':
            printf("v0.1 \n");
            exit(1);
        case 'h':
            Usage(argv[0]);
            exit(1);
            f =optarg;            
            break;
        }
    }
}

int main(int argc,char * argv[])
{    
    int res =1;
    do{
        std::string confFile;
        GetOptions(argc,argv,confFile);
        
        lmq::Context context;
        
        lmq::Config conf;
        if(0!=context.SetConf(&conf))
        {
            printf("context.SetConf failed! \n");
            break;
        }
        
        if(0!=conf.Reload(confFile.c_str()))
        {
            printf("Reload config file failed! %s \n",confFile.c_str());
            break;
        }        
        LOG_INFO("base","lmq starting ...... ");
        
        lmq::NetworkService networkService;
        networkService.Start(&context);
        res =0;
    }while(0);
    return res;
}

