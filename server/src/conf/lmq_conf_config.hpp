#ifndef _HEADER_FILE_LMQ_CONF_CONFIG_HPP_
#define _HEADER_FILE_LMQ_CONF_CONFIG_HPP_

#include "lpr/lpr_conf_parser.hpp"

namespace lmq
{

class Config
{
public:
    // mainten addr . default is "127.0.0.1:18601".
    std::string mainten_addr;
    // client addr . default is "127.0.0.1:9601".
    std::string client_addr;
    // service addr . default is "127.0.0.1:8601".
    std::string service_addr;
    // log_conf_file addr . default is "../res/litemq-log.properties".
    std::string log_conf_file;

public:
    Config();
    ~Config();

    int Reload(const std::string & confFile);
    int SaveToFile();
private:
    lpr::ConfParser * _confParser;
};

} // namespace lmq

#endif // _HEADER_FILE_LMQ_CONF_CONFIG_HPP_
