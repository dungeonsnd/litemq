
#include "conf/lmq_conf_config.hpp"
#include "lpr/lpr_common_helper_macro.hpp"
#include "lpr/lpr_common_logger.hpp"

namespace lmq
{

Config::Config():_confParser(NULL)
{
    _confParser =new lpr::ConfParser();
}

Config::~Config()
{
    delete _confParser;
    _confParser =NULL;
}

int Config::Reload(const std::string & confFile)
{
    int res =-1;
    do{
//        std::string conf_file="/etc/litemq.conf";
        std::string conf_file="../res/litemq.conf";
        if(!confFile.empty())
            conf_file=confFile;
        if(0!=_confParser->Parse(conf_file))
        {
            break;
        }

        mainten_addr ="127.0.0.1:18601";
        _confParser->GetValue("mainten_addr", mainten_addr); 
        
        client_addr ="0.0.0.0:9601";   
        _confParser->GetValue("client_addr", client_addr); 
            
        service_addr ="0.0.0.0:8601";
        _confParser->GetValue("service_addr", service_addr); 
        
//        log_conf_file="/etc/litemq-log.properties";
        log_conf_file="../res/litemq-log.properties";
        _confParser->GetValue("log_conf_file", log_conf_file);
        LOG_INIT(log_conf_file.c_str());

        res =0;
    }while(0);
    return res;
}

int Config::SaveToFile()
{
    int res =-1;
    do{
        if(0!=_confParser->SaveToFile())
        {
            break;
        }
        
        res =0;
    }while(0);
    return res;
}


} // namespace lmq

