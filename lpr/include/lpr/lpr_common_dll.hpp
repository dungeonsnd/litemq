#ifndef _HEADER_FILE_LPR_COMMON_HELPER_DLL_HPP_
#define _HEADER_FILE_LPR_COMMON_HELPER_DLL_HPP_

void * lpr_common_dll_get_handle(const char *filename);

void lpr_common_dll_release_handle(void * handle);

void * lpr_common_dll_get_func(void * handle, const char *funcName);

#endif // _HEADER_FILE_LPR_COMMON_HELPER_DLL_HPP_
