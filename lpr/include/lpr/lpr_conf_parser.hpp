#ifndef _HEADER_FILE_LPR_CONF_PARSER_HPP_
#define _HEADER_FILE_LPR_CONF_PARSER_HPP_

#include <string>
#include <map>
#include <fstream>

namespace lpr
{

class ConfParser
{
public:
    ConfParser();
    ~ConfParser();

    int Parse(const std::string & confFile);
    int GetValue(const std::string & key, std::string & value);
    void SetValue(const std::string & key, const std::string & value);
    int SaveToFile();

private:
    std::map < std::string ,std::string > _config;
    std::string _confFile;
};

} // namespace lpr

#endif // _HEADER_FILE_LPR_CONF_PARSER_HPP_
