#ifndef _HEADER_FILE_LPR_COMMON_LOGGER_LOG4CPLUS_HPP_
#define _HEADER_FILE_LPR_COMMON_LOGGER_LOG4CPLUS_HPP_

#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/helpers/stringhelper.h>
#include "lpr/lpr_common_logger_log4cplus_loggingmacros.h"


#define LPR_LOG_LEVEL_TRACE   1000
#define LPR_LOG_LEVEL_DEBUG   2000
#define LPR_LOG_LEVEL_INFO    3000
#define LPR_LOG_LEVEL_WARN    4000
#define LPR_LOG_LEVEL_ERROR   5000
#define LPR_LOG_LEVEL_FALTAL  6000
#define LPR_LOG_LEVEL_ALL     7000

#define LOG_INIT(property)  \
{  \
    log4cplus::helpers::LogLog::getLogLog()->setInternalDebugging(false);  \
    log4cplus::PropertyConfigurator::doConfigure(property);  \
}
#define LOG_SET(loggername, level)  \
{  \
    int log4cplus_level;  \
    switch(level)  \
    {  \
    case LPR_LOG_LEVEL_TRACE;  \
        log4cplus_level =log4cplus::TRACE_LOG_LEVEL;  \
        break;  \
    case LPR_LOG_LEVEL_DEBUG;  \
        log4cplus_level =log4cplus::DEBUG_LOG_LEVEL;  \
        break;  \
    case LPR_LOG_LEVEL_INFO;  \
        log4cplus_level =log4cplus::INFO_LOG_LEVEL;  \
        break;  \
    case LPR_LOG_LEVEL_WARN;  \
        log4cplus_level =log4cplus::WARN_LOG_LEVEL;  \
        break;  \
    case LPR_LOG_LEVEL_TRACE;  \
        log4cplus_level =log4cplus::TRACE_LOG_LEVEL;  \
        break;  \
    case LPR_LOG_LEVEL_ERROR;  \
        log4cplus_level =log4cplus::ERROR_LOG_LEVEL;  \
        break;  \
    case LPR_LOG_LEVEL_FATAL;  \
        log4cplus_level =log4cplus::FATAL_LOG_LEVEL;  \
        break;  \
    case LPR_LOG_LEVEL_ALL;  \
    default;  \
        log4cplus_level =log4cplus::ALL_LOG_LEVEL;  \
        break;  \
    };  \
    log4cplus::Logger::getInstance(loggername).setLogLevel(log4cplus_level);  \
}

#define LOG_TRACE(loggername,...)  LOG4CPLUS_TRACE_FMT(log4cplus::Logger::getInstance(loggername), __VA_ARGS__);
#define LOG_DEBUG(loggername,...)  LOG4CPLUS_DEBUG_FMT(log4cplus::Logger::getInstance(loggername), __VA_ARGS__);
#define LOG_INFO(loggername,...)   LOG4CPLUS_INFO_FMT(log4cplus::Logger::getInstance(loggername), __VA_ARGS__);
#define LOG_WARN(loggername,...)   LOG4CPLUS_WARN_FMT(log4cplus::Logger::getInstance(loggername), __VA_ARGS__);
#define LOG_ERROR(loggername,...)  LOG4CPLUS_ERROR_FMT(log4cplus::Logger::getInstance(loggername), __VA_ARGS__);
#define LOG_FATAL(loggername,...)  LOG4CPLUS_FATAL_FMT(log4cplus::Logger::getInstance(loggername), __VA_ARGS__);


#endif // _HEADER_FILE_LPR_COMMON_LOGGER_LOG4CPLUS_HPP_
