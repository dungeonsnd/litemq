#ifndef _HEADER_FILE_LPR_SESSION_DEFINE_HPP_
#define _HEADER_FILE_LPR_SESSION_DEFINE_HPP_

#include "lpr/lpr_common_include_libevent.hpp"
#include "lpr/lpr_common_include_cpp.hpp"
#include "lpr/lpr_common_helper_macro.hpp"

namespace lpr
{

typedef void (* lpr_sesssion_destructor_cb) (void * context, void * data, void * packet);

class Session
{
public:
    enum RWFLAG
    {
        RWF_R =0,
        RWF_W =1
    };

public:
    static lpr_sesssion_destructor_cb _destructorCb;
    
    Session(event_base * base,
            evutil_socket_t clientsocket,
            void * context,
            void * sessionInfo,
            void * packet,
            bufferevent_data_cb readcb,
            bufferevent_data_cb writecb,
            bufferevent_event_cb eventcb);
    ~Session();

    event_base * GetBase() { return bufferevent_get_base(_bev); }
    bufferevent * GetBev() { return _bev; }
    void * GetContext() { return _context; }
    void * GetSessionInfo() { return _sessionInfo; }
    void * GetPacket() { return _packet; }

    int GetBufSize(RWFLAG flag, char errmsg[LPR_ERR_SIZE]);
    int Read(char * buf, size_t size, char errmsg[LPR_ERR_SIZE]);
    int Read(std::string & data, size_t size, char errmsg[LPR_ERR_SIZE]);
    int Write(const char * buf, size_t size, char errmsg[LPR_ERR_SIZE]);
    
private:
    bufferevent * _bev;
    void * _context;
    void * _sessionInfo;
    void * _packet;
};

} // namespace lpr

#endif // _HEADER_FILE_LPR_SESSION_DEFINE_HPP_
