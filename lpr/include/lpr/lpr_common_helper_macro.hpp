#ifndef _HEADER_FILE_LPR_COMMON_HELPER_MACRO_HPP_
#define _HEADER_FILE_LPR_COMMON_HELPER_MACRO_HPP_

#include "lpr/lpr_common_include_cpp.hpp"

#define LPR_ERR_SIZE 2048

#define LPR_ISNULL(obj,errmsg) \
        if(NULL==obj)  \
        {  \
            snprintf(errmsg,LPR_ERR_SIZE,"NULL==%s,%s,%s:%d  ",#obj,__FUNCTION__,__FILE__,__LINE__);  \
            LOG_WARN("base",errmsg);  \
            break;  \
        }

#define LPR_STR_EQ(a,b) ( 0==strncmp(a,b,strlen(a)) )


template <typename Type>
inline Type * lmq_new()
{
    Type * p = NULL;
    try
    {
        p = new Type;
    }
    catch (std::bad_alloc & ba)
    {
        p = NULL;
    }
    return p;
}

#define lmq_newobj(Pointer, ClassName, ...) \
    ClassName * Pointer=NULL; \
    try \
    { \
        Pointer = new ClassName(__VA_ARGS__); \
    } \
    catch (std::bad_alloc& ba) \
    { \
        Pointer = NULL; \
    } \
    
#endif // _HEADER_FILE_LPR_COMMON_HELPER_MACRO_HPP_
