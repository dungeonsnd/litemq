#ifndef _HEADER_FILE_LPR_COMMON_NETWORK_HPP_
#define _HEADER_FILE_LPR_COMMON_NETWORK_HPP_

#include "lpr/lpr_common_include_libevent.hpp"

namespace lpr
{

int lpr_common_create_listensocket(const char * addr, evutil_socket_t & listensocket);

int lpr_common_accept(evutil_socket_t listensocket,evutil_socket_t & clientsocket);

} // namespace lpr

#endif // _HEADER_FILE_LPR_COMMON_NETWORK_HPP_

