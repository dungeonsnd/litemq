
#include "lpr/lpr_common_dll.hpp"
#include "lpr/lpr_common_include_system.hpp"

#include <stdlib.h>
#include <stdio.h>

void * lpr_common_dll_get_handle(const char *filename)
{
    void * handle =dlopen(filename, RTLD_LAZY);
    if (!handle) {
        fprintf(stderr, "Failed dlopen, %s\n", dlerror());
    }
    dlerror();
    return handle;
}

void lpr_common_dll_release_handle(void * handle)
{
    int rt =dlclose(handle);
    if (0!=rt) {
        fprintf(stderr, "Failed dlclose, %s\n", dlerror());
    }
}


void * lpr_common_dll_get_func(void * handle, const char *funcName)
{
    void * p =dlsym(handle, funcName);
    if (NULL==p) {
        fprintf(stderr, "Failed dlsym, %s\n", dlerror());
    }
    return p;
}

