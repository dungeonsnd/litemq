
#include "lpr/lpr_conf_parser.hpp"
#include "lpr/lpr_common_logger.hpp"

namespace lpr
{

ConfParser::ConfParser()
{
}

ConfParser::~ConfParser()
{
    _config.clear();
}

int ConfParser::Parse(const std::string & confFile)
{
    int res =-1;
    do{
        _confFile =confFile;
        std::ifstream fs;
        fs.open(_confFile.c_str(),std::ios::in);
        if(!fs)
        {
            LOG_ERROR("base","open file failed! %s \n",_confFile.c_str());
            break;
        }
        
        std::string line;
        std::string k,v;
        size_t indexEq;
        while(std::getline(fs,line))
        {
            if (!line.length()) continue;
            if ('#'==line[0] || ';'==line[0]) continue;
            
            indexEq =line.find('=');
            k =line.substr(0,indexEq);
            v =line.substr(indexEq+1);
            
            size_t index =v.find('\r');
            if(index!=std::string::npos)
                v =v.substr(0,index);
            index =v.find('\n');
            if(index!=std::string::npos)
                v =v.substr(0,index);
            
            _config[k] =v;
        }
        //int fileNotFound =_config.empty();
        fs.close();
        res =0;
    }while(0);
    return res;
}

int ConfParser::GetValue(const std::string & key, std::string & value)
{
    std::map < std::string ,std::string >::const_iterator it=_config.find(key);
    if(it!=_config.end())
    {
        value =it->second;
        return 0;
    }
    return -1;
}

void ConfParser::SetValue(const std::string & key, const std::string & value)
{
    std::map < std::string ,std::string >::iterator it=_config.find(key);
    if(it!=_config.end())
        it->second =value;
}

int ConfParser::SaveToFile()
{
    int res =-1;
    do{
        std::ofstream of(_confFile.c_str());
        of.open(_confFile.c_str(),std::ios::out);
        if(!of)
        {
            break;
        }
        
        std::map < std::string ,std::string >::iterator it=_config.begin();
        for(;it!=_config.end();++it)
        {
            std::string kv =it->first+"="+it->second;
            of<<kv<<std::endl;
        }
        of.close();
        res =0;
    }while(0);
    return res;
}

} // namespace lpr

