
#include "lpr/lpr_common_network.hpp"
#include "lpr/lpr_common_include_system.hpp"
#include "lpr/lpr_common_helper_macro.hpp"
#include "lpr/lpr_common_logger.hpp"


namespace lpr
{

int lpr_common_create_listensocket(const char * addr, evutil_socket_t & listensocket)
{
    int res =-1;
    do{
        sockaddr outaddr;
        int outlen =int(sizeof(outaddr));
        if(0!=evutil_parse_sockaddr_port(addr, &outaddr, &outlen))
        {
            printf("Failed evutil_parse_sockaddr_port! \n");
            break;
        }

        listensocket = socket(AF_INET, SOCK_STREAM, 0);
        evutil_make_socket_nonblocking(listensocket);


        if (bind(listensocket, &outaddr, sizeof(outaddr)) < 0)
        {
            printf("Failed bind \n");
            break;
        }
        if (listen(listensocket, 1024)<0)
        {
            printf("Failed listen \n");
            break;
        }

        res =0;
    }while(0);
    return res;
}

int lpr_common_accept(evutil_socket_t listensocket,evutil_socket_t & clientsocket)
{
    LOG_TRACE("base","Enter lpr_common_accept, listensocket=%d",listensocket);
    int res =-1;
    do{
#ifdef _UNIX
        sockaddr_in peerAddr;
        socklen_t slen =sizeof(peerAddr);
        clientsocket =accept(listensocket,(sockaddr*)&peerAddr,&slen);
        if(clientsocket<0)
        {
            printf("---- clientsocket =%d \n",clientsocket);
            if(errno==EAGAIN||errno==EWOULDBLOCK) // Not error!
            {
            }
            else // Error!
            {
            }
            break;
        }
#endif 
        res =0;
    }while(0);
    LOG_TRACE("base","Leave lpr_common_accept, res=%d, clientsocket=%d",res, clientsocket);
    return res;
}

} // namespace lpr

