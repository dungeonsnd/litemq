
#include "lpr/lpr_session_define.hpp"

namespace lpr
{


lpr_sesssion_destructor_cb Session::_destructorCb =NULL;

Session::Session(event_base * base,
                evutil_socket_t clientsocket,
                void * context,
                void * sessionInfo,
                void * packet,
                bufferevent_data_cb readcb,
                bufferevent_data_cb writecb,
                bufferevent_event_cb eventcb):
            _bev(NULL),
            _context(context),_sessionInfo(sessionInfo),_packet(packet)
{
    evutil_make_socket_nonblocking(clientsocket);
    _bev =bufferevent_socket_new(base,clientsocket,BEV_OPT_CLOSE_ON_FREE); 
    
    bufferevent_setcb(_bev,readcb,writecb,eventcb,this);
    bufferevent_enable(_bev,EV_READ|EV_WRITE);

    timeval tv ={7200,0};
    bufferevent_set_timeouts(_bev,&tv,NULL);
}

Session::~Session()
{
    if(_bev)
        bufferevent_free(_bev);    
    if(_destructorCb)
    {
        _destructorCb(_context, _sessionInfo, _packet);
    }
}

int Session::GetBufSize(RWFLAG flag, char errmsg[LPR_ERR_SIZE])
{
    int res =-1;
    do{
        if(NULL==_bev)
        {
            snprintf(errmsg,LPR_ERR_SIZE,"Session::GetBufSize, NULL==_bev.");
            break;
        }
        
        evbuffer *buffer = NULL;
        if(RWF_R==flag)
        {
            buffer =bufferevent_get_input(_bev);
        }
        else if(RWF_W==flag)
        {
            buffer =bufferevent_get_output(_bev);
        }
        else
        {
            snprintf(errmsg,LPR_ERR_SIZE,"Session::GetBufSize, flag{%d} error.",
                flag);
            break;
        }
        
        if(NULL==_bev)
        {
            snprintf(errmsg,LPR_ERR_SIZE,"Session::GetBufSize, failed bufferevent_get_input.");
            break;
        }
        
        // 没有做数据缓冲区大小限制,所以有溢出的风险. 
        // TODO: bufferevent_disable(_bev) when reach BUFSIZE_LIMIT, and bufferevent_enable after read some data from buf.
        res =int(evbuffer_get_length(buffer));
    }while(0);
    return res;
}

int Session::Read(char * buf, size_t size, char errmsg[LPR_ERR_SIZE])
{
    int res =-1;
    do{
        if(NULL==_bev)
        {
            snprintf(errmsg,LPR_ERR_SIZE,"Session::Read, NULL==_bev.");
            break;
        }
        
        res =int(bufferevent_read(_bev, buf, size));
    }while(0);
    return res;
}

int Session::Read(std::string & data, size_t size, char errmsg[LPR_ERR_SIZE])
{
    int res =-1;
    do{
        if(NULL==_bev)
        {
            snprintf(errmsg,LPR_ERR_SIZE,"Session::Read, NULL==_bev.");
            break;
        }
        
        if(size<1)
        {
            res =0;
            break;
        }
        
        size_t orginalSize =data.size();
        data.resize(orginalSize+size);
        res =int(bufferevent_read(_bev, &data[orginalSize], size));
    }while(0);
    return res;
}

int Session::Write(const char * buf, size_t size, char errmsg[LPR_ERR_SIZE])
{
    int res =-1;
    do{
        if(NULL==_bev)
        {
            snprintf(errmsg,LPR_ERR_SIZE,"Session::Write, NULL==_bev.");
            break;
        }
        
        if(0!=bufferevent_write(_bev, buf, size))
        {
            snprintf(errmsg,LPR_ERR_SIZE,"Session::Write, bufferevent_write failed.");
            break;
        }
        
        res =0;
    }while(0);
    return res;
}

} // namespace lpr

