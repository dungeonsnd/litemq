# litemq
**a lite message queue**
**一个超轻量级的消息队列**


	sponsor: jeffery
	email: dungeonsnd@126.com or dungeons@gmail.com

===================================

	市面上有较多mq,比较常用的开源产品有 activemq,rabbitmq,zeromq,rocketmq. 
	作者曾经打算使用其中的某些产品,但发现他们有些特性不能满足我的需求. 
	而且其中的一些产品为了适应更多的场合,实现了非常复杂的特性和规范,
	功能特别强大,但是同时增加了使用和调试的复杂性.
	
	本项目目标是做一个最简的mq,功能极其单一.可能和其它的mq相比比较简陋,
	但是适用和够用就好,作者就想提供这样的一个产品供有同样需求的开发者使用,
	以期避免重复造轮子.

	我们追求的是极速性能,简洁易用,稳定安全,多语言支持(c/c++,java,python,object-c). 
	这势必会失去一些特定项目中不太关心的特性,如集群功能等.



1  Message handling
-----------------------------------
	A --> litemq0 --network--> litemq1 --> B
	生产者 --> mq0 --> 网络 --> mq1 --> 消费者
	1.1 User A want to send a message to B.
	1.2 A call a litemq method to send the message to litemq0 process.
	1.3 litemq0 send the message to litemq1 over network.
	1.4 litemq1 send a receipt to litemq0.
	1.5 litemq0 resend the message to litemq1 over network if hasn't recved a receipt.
	1.6 litemq1 notify B to get the message.
	1.7 B get the message.
		
2 Features
-----------------------------------
	2.1 retransmission 重传
	未收到回执的消息会自动重传,重传的时间和次数可配置. 
	
	2.2 order 顺序 (可配置成重传不保证顺序性)
	发送和收到的消息保证顺序性,即使中间某条消息被重传,也会保证顺序性. 
	
	2.3 reliability 可靠
	消息的可靠传递.即未收到回执自动重传, 成功和失败都会有异步通知. 
	消息可配置成持久化,mq崩溃重启,消息不会丢失. 
	是否使用回执来保证消息的可靠性待定，其它机制考虑之中。 
	
	2.4 smooth upgrade
	litemq为独立运行的应用,该应用可平滑升级,让用户无感知的情况下进行litemq升级. 
	
	2.5 filter & handler
	可以配置多个过滤器和处理器. 如压缩,加密等处理插件. 
	
	2.6 pub/sub & PTP
	支持订阅模式(pub/sub)及采用PTP(point to point)模式.

3 Design
-----------------------------------
	3.1 message_id
	mq传递是基于消息而不是流.
	所有的消息都有全局唯一的消息标识号,即messge_id. 默认使用16字节guid.
	
	3.2 channel_id
	所有的连接都有全局唯一的标识,称为通道标识号,即channel_id. 默认使用16字节guid.
	
	3.3 transmission_time
	每条消息都会记录传输开始时刻,接收时刻,传输次数等.
	
	3.4 传输连接
	
	每个mq启动时会被指定本机唯一名称(mq_name),
	绑定两个侦听端口,默认为 0.0.0.0:8601(与其它mq通信), 127.0.0.1:18601(与调用者通信).
	启动时端口被占用自动递增.
	
	生产者/消费者会通过sdk的注册接口向其mq发起tcp长连接,sdk根据mq_name进行连接.
	生产者/消费者与其mq之间通过此连接发消息/收消息/异步通知,解注册时断开连接. 
	生产者/消费者应该在自己的事件循环中监听中此连接的事件,并针对消息和通知做对应处理.	

	注册后,mq会把消息的到达通知异步传达(目前单机使用tcp方式)给该消费者,
	尔后该消费者调用lmq_recv从mq取出消息. 
	
	3.5 保活机制
	生产者的mq和消费者的mq之间会通过流量极小的双向心跳来保活, 消费者的mq心跳超时后,
	生产者mq会删除之前的注册信息,然后重选一消费者的mq来接管, 
	心跳超时时间内的消息通过重发来保证能继续发给另一消费者的mq. 
		
	3.6 PTP模式
	配置PTP模式时, mq对于同一连接(channel_id)的消息会发给消费者的mq. 
	mq根据配置的路由策略(如round_robin)进行分发,
	维护一份channel_id与消费者的mq一一对应关系(持久化以防重启).
	
4 Interfaces(c/c++/java/python)
-----------------------------------
	本着简洁的原则,作者设计了如下的4类接口,
	以类c++(因提供多语言支持的sdk,所以不同语言的接口会不同)方式来描述,示意如下,
	
	4.1 注册/解注册接口
	int lmq_register_sender(out_chinfo); 
	int lmq_unregister_sender(out_chinfo);
	int lmq_register_recver([in_chid],out_chinfo); 
	int lmq_unregister_recver([in_chid],out_chinfo);
	调用 lmq_register_sender 向mq注册一个发送者/生产者(sender/producer).
	调用 lmq_unregister_sender 向mq解注册一个发送者/生产者(sender/producer).
	调用 lmq_register_recver 向mq注册一个服务/消费者/接收者(service/recver/consumer).
	调用 lmq_unregister_recver 向mq解注册一个服务/消费者/接收者(service/recver/consumer).

	
	4.2 发送消息接口
	int lmq_send(in_msgobj, [in_destip,] [in_destport,] out_msginfo);
	向mq投递一条消息. 入参消息对象, 入参目标ip可选, 入参目标端口号可选, 出参消息属性.
	
	4.3 接收消息接口
	int lmq_recv([in_chid,] out_msgobj, out_msginfo);
	从mq取一条消息. 入参通道标识可选, 出参消息对象, 出参消息属性.

	4.4 通知接口 (notification interfaces)
	 1)  sent successfully
	mq发送成功.意味着对端的mq收到了此消息.
	 2)  sent failed
	mq多次尝试发送都失败了.意味着对端的mq没有收到此消息.
	 3)  retrying
	mq尝试重新发送.意味着上一次发送失败,对端的mq没有收到此消息.
	 4)  recved message
	mq收到n条消息,用户应该调用lmq_recv去取出这些消息.


5 使用过程示意
-----------------------------------
	1) 生产者:
	lmq_send(msg0) -> notifications(msg0 sent successfully) ----> 
	lmq_send(msg1) -> notifications(msg1 retrying), mq auto restransmit when timeout -> 
	notifications(msg1 retrying), mq auto restransmit when timeout again -> 
	notifications(msg1 sent failed)

	重传可配置,默认3次,与上次传输间隔分别为2/20/30秒,
	这是尽快排除失败和对适时故障的容忍性之间的一对矛盾. 
	严格顺序模式下,重传最终失败会记录并丢弃该消息,然后传输下一条消息.
	非严格顺序模式下,重传的同时传输下一条消息.

	2) 消费者:
	lmq_register_service() -> notifications(recved message) -> lmq_recv(msg0)






